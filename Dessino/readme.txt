DESSINO - Application de dessin interactif

DESSINO est une application de dessin interactive réalisée en utilisant le module tkinter de Python. L'application permet aux utilisateurs de dessiner librement sur un canevas, de choisir des couleurs et des largeurs de traits, ainsi que de générer aléatoirement des mots à dessiner avec des exemples d'images associées.

Fonctionnalités

- Dessin libre sur un canevas
- Choix de couleurs et de largeurs de trait
- Effacement du dessin avec une gomme
- Génération aléatoire de mots à dessiner avec des exemples d'images associées
- Sauvegarde du dessin sous forme d'image PNG

Prérequis

- Python 3.x
- Les modules suivants doivent être installés :
  - tkinter
  - Pillow (PIL)

Installation

1. Clonez ce dépôt :
https://github.com/votre-utilisateur/votre-repo.git