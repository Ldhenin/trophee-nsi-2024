import tkinter as tk
from tkinter.colorchooser import askcolor
from tkinter import simpledialog
import random
from tkinter import messagebox
from tkinter import filedialog
from PIL import ImageGrab
from tkinter import Toplevel, Label, PhotoImage
from PIL import Image, ImageTk

class HomePage:
    def __init__(self, root):
        self.root = root
        self.root.title("DESSINO")
        self.root.attributes("-fullscreen", True)  # Lancer en mode plein écran
        self.root.geometry("{}x{}".format(root.winfo_screenwidth(), root.winfo_screenheight()))  # Définir la taille de la fenêtre

        # Créer une étiquette pour le titre "DESSINO" avec un style calligraphique
        title_font = ("Script MT Bold", 200)  # Utilisez une police calligraphique (ex. Script MT Bold)
        title_label = tk.Label(root, text="DESSINO", font=title_font, fg="pink")
        title_label.pack(pady=250)

        # Créer une étiquette pour la phrase "C'est parti"
        start_label = tk.Label(root, text="C'est parti", font=("Helvetica", 24), fg="black", cursor="hand2")
        start_label.pack(pady=20)
        start_label.bind("<Button-1>", self.open_paint_app)

    def open_paint_app(self, event):
        # Fermer la fenêtre de la page d'accueil
        self.root.destroy()

        # Ouvrir l'application de dessin
        root = tk.Tk()
        app = PaintApp(root)
        root.mainloop()

    def exit_fullscreen(self):
        # Quitter le mode plein écran en appuyant sur "Échap"
        self.root.attributes("-fullscreen", False)

class PaintApp:
    # Définir les exemples d'images pour certains mots
    word_examples = {
        "Chat": ["ex/tchat1.png", "ex/tchat2.png", "ex/tchat3.png", "ex/tchat4.png", "ex/tchat5.png"],
        "Chien": ["ex/tchien1.png", "ex/tchien2.png", "ex/tchien4.png", "ex/tchien5.png", "ex/tchien3.png"],
        "Maison": ["ex/maison1.png", "ex/maison2.png", "ex/maison3.png", "ex/maison4.png", "ex/maison5.png"],
        "Arbre": ["ex/arbre1.png", "ex/arbre2.png", "ex/arbre3.png", "ex/arbre4.png", "ex/arbre5.png"],
        "Fleur": ["ex/fleur1.png", "ex/fleur2.png", "ex/fleur3.png", "ex/fleur4.png", "ex/fleur5.png"],
        "Voiture": ["ex/voiture1.png", "ex/voiture2.png", "ex/voiture3.png", "ex/voiture4.png", "ex/voiture5.png"],
        "Soleil": ["ex/soleil1.png", "ex/soleil2.png", "ex/soleil3.png", "ex/soleil4.png", "ex/soleil5.png"],
        # Ajoutez des exemples pour d'autres mots si nécessaire
    }

    def show_word_example(self, word):
        if word in self.word_examples:
            example_images = self.word_examples[word]
            example_window = Toplevel(self.root)
            example_window.title("Exemples pour le mot : " + word)
            for image_path in example_images:
                pil_image = Image.open(image_path)
                pil_image = self.resize_image(pil_image)  # Redimensionner l'image
                tk_image = ImageTk.PhotoImage(pil_image)
                label = Label(example_window, image=tk_image)
                label.image = tk_image
                label.pack()
                label.bind("<Button-1>", lambda event, img=tk_image: self.display_image_on_canvas(img))

    def display_image_on_canvas(self, image):
        self.canvas.delete("all")
        # Obtenir les dimensions de l'image
        image_width = image.width()
        image_height = image.height()
        # Définir une taille maximale pour l'affichage sur le canevas (en proportion de l'original)
        max_canvas_width = min(image_width * 2, self.canvas.winfo_width())
        max_canvas_height = min(image_height * 2, self.canvas.winfo_height())
        # Afficher l'image sur le canevas
        self.canvas.create_image(10, self.canvas.winfo_height() / 2, anchor="w", image=image, tags="image")
        # Redimensionner l'image sur le canevas si elle dépasse la taille maximale
        self.canvas.scale("image", 0, 0, 1, 1, min(max_canvas_width / image_width, max_canvas_height / image_height))

    def resize_image(self, pil_image, max_size=(100, 100)):
        pil_image.thumbnail(max_size, Image.ANTIALIAS)
        return pil_image

    def save_canvas(self):
        file_path = filedialog.asksaveasfilename(defaultextension=".png", filetypes=[("PNG files", "*.png"), ("All Files", "*.*")])
        if file_path:
            x0 = self.canvas.winfo_rootx() + self.canvas.winfo_x()
            y0 = self.canvas.winfo_rooty() + self.canvas.winfo_y()
            x1 = x0 + self.canvas.winfo_width()
            y1 = y0 + self.canvas.winfo_height()
            ImageGrab.grab().crop((x0, y0, x1, y1)).save(file_path)

    def quit_app(self):
        self.root.destroy()

    def __init__(self, root):
        self.root = root
        self.root.title("Paint App")
        self.root.attributes("-fullscreen", False)  # Lancer en mode plein écran
        self.root.geometry("{}x{}".format(root.winfo_screenwidth(), root.winfo_screenheight()))  # Définir la taille de la fenêtre

        # Créer un canevas pour dessiner
        self.canvas = tk.Canvas(root, bg="white")
        self.canvas.pack(fill="both", expand=True)

        # Créer une barre de menu
        menubar = tk.Menu(root)
        root.config(menu=menubar)

        # Menu "Fichier"
        file_menu = tk.Menu(menubar)
        menubar.add_cascade(label="Fichier", menu=file_menu)
        file_menu.add_command(label="Effacer", command=self.clear_canvas)
        file_menu.add_separator()
        file_menu.add_command(label="Sauvegarder", command=self.save_canvas)
        file_menu.add_separator()
        file_menu.add_command(label="Quitter", command=self.quit_app)

        # Menu "Options"
        options_menu = tk.Menu(menubar)
        menubar.add_cascade(label="Options", menu=options_menu)
        options_menu.add_command(label="Changer de couleur", command=self.choose_color)
        options_menu.add_command(label="Changer la largeur du trait", command=self.change_line_width)

        # Initialisation des variables
        self.last_x, self.last_y = None, None
        self.drawing = False
        self.eraser_mode = False
        self.selected_color = "black"
        self.line_width = 2

        # Créer un cadre pour les boutons en haut
        button_frame = tk.Frame(root)
        button_frame.pack(side="top", fill="x")

        # Créer des boutons pour la gomme, le stylo et l'effacement, ....
        self.eraser_button = tk.Button(button_frame, text="Gomme", command=self.toggle_eraser, bg="lightgray")
        self.eraser_button.pack(side="left", padx=10, pady=10)

        self.color_button = tk.Button(button_frame, text="Changer de couleur", command=self.choose_color, bg="lightblue")
        self.color_button.pack(side="left", padx=10, pady=10)

        self.line_width_button = tk.Button(button_frame, text="Changer la largeur du trait", command=self.change_line_width, bg="lightgreen")
        self.line_width_button.pack(side="left", padx=10, pady=10)

        self.clear_button = tk.Button(button_frame, text="Effacer tout", command=self.clear_canvas, bg="salmon")
        self.clear_button.pack(side="left", padx=10, pady=10)

        self.generate_word_button = tk.Button(button_frame, text="Générer un mot", command=self.generate_word, bg="lightyellow")
        self.generate_word_button.pack(side="left", padx=10, pady=10)

        exit_fullscreen_button = tk.Button(root, text="Sauvegarder", command=self.save_canvas, bg="white")
        exit_fullscreen_button.pack(side="right", padx=10, pady=10)

        exit_fullscreen_button = tk.Button(root, text="Quitter le mode plein écran", command=self.exit_fullscreen, bg="white")
        exit_fullscreen_button.pack(side="right", padx=10, pady=10)

        exit_fermer_fenetre_button = tk.Button(root, text="Quitter la page", command=self.root.destroy, bg="white")
        exit_fermer_fenetre_button.pack(side="right", padx=10, pady=10)


        self.word_list = ["Chat", "Chien", "Maison", "Arbre", "Fleur", "Voiture", "Soleil"]

        # Lier les événements de dessin au canevas
        self.canvas.bind("<Button-1>", self.start_drawing)
        self.canvas.bind("<B1-Motion>", self.draw)
        self.canvas.bind("<ButtonRelease-1>", self.stop_drawing)

    def start_drawing(self, event):
        self.last_x, self.last_y = event.x, event.y
        self.drawing = True

    def draw(self, event):
        if self.drawing and not self.eraser_mode:
            x, y = event.x, event.y
            if self.last_x and self.last_y:
                self.canvas.create_line(self.last_x, self.last_y, x, y, fill=self.selected_color, width=self.line_width)
            self.last_x, self.last_y = x, y
        elif self.drawing and self.eraser_mode:
            x, y = event.x, event.y
            self.canvas.create_rectangle(x - 10, y - 10, x + 10, y + 10, fill="white", outline="white")

    def stop_drawing(self, event):
        self.drawing = False

    def toggle_eraser(self):
        self.eraser_mode = not self.eraser_mode
        if self.eraser_mode:
            self.eraser_button.config(text="Stylo")
        else:
            self.eraser_button.config(text="Gomme")

    def choose_color(self):
        color = askcolor()[1]  # Demande à l'utilisateur de choisir une couleur
        if color:
            self.selected_color = color

    def change_line_width(self):
        self.line_width = simpledialog.askinteger("Largeur du trait", "Entrez la largeur du trait (en pixels):", initialvalue=self.line_width)

    def clear_canvas(self):
        self.canvas.delete("all")

    def generate_word(self):
        selected_word = random.choice(self.word_list)
        messagebox.showinfo("Mot à dessiner", f"Dessinez le mot : {selected_word}")
        # Afficher l'exemple pour le mot généré
        self.show_word_example(selected_word)

    def exit_fullscreen(self):
        self.root.attributes("-fullscreen", False)

if __name__ == "__main__":
    root = tk.Tk()
    app = HomePage(root)
    root.mainloop()

